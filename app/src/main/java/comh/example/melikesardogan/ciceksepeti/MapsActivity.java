package comh.example.melikesardogan.ciceksepeti;


import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Bu class harita aktivitelerinin bazı işlemlerini gerçekleştirerek yapacak olan classtır
 * @author nullPointerException
 * @since 03.2019
 */

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    /**
     * map
     */
    private GoogleMap mMap;
    /**
     * Şube classından oluşturulan nesnelerin atılacağı liste
     * Şubeler bu listede tutularak şube ile ilgili tüm işlemler için bu liste kullanılır
     */
    private List<Branches> branchesList = new ArrayList<>();
    /**
     * Siparişler için oluşturulan Order classından oluşturulan objelerin tutulacağı liste
     */
    private List<Orders> ordersList = new ArrayList<>();
    Context context;
    Branches redBranch;
    Branches blueBranch;
    Branches greenBranch;
    /**
     * Kırmızı şubeden dağıtılacak olan siparişlerin tutulduğu liste
     */
    List<Orders> ordersListRed;
    /**
     * Yeşil şubeden dağıtılacak olan siparişlerin tutulduğu liste
     */
    List<Orders> ordersListGreen;
    /**
     * Mavi şubeden dağıtılacak olan siparişlerin tutulduğu liste
     */
    List<Orders> ordersListBlue;
    /**
     * Extraların (minimum siparişler dağıtıldıktan sonra kalan siparişler) tutulduğu liste
     */
    List<Orders> checkList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        context = this;

        // Exceldeki koorinatları okuyabilmek için path oluşturulur.
        InputStream inputStream = getResources().openRawResource(R.raw.sipariskoordinat);
        // ReadFromExcel classından bir obje oluşturulur.
        ReadFromExcel readFromExcel = new ReadFromExcel(inputStream);

        // obje üzerine exceldeki değerler tek tek atılır
        Object[][] tempData = readFromExcel.getData();

        //Exceldeki değerler, Orders classından obje oluşturularak obje içerisine gömülür.
        //Tüm objelerde ordersList ismindeki listeye atılır
        for (int i = 0; i < tempData.length; i++) {
            Orders orders = new Orders();
            orders.setId(((BigDecimal) tempData[i][0]).intValue());
            orders.setLat(((BigDecimal) tempData[i][1]).doubleValue());
            orders.setLon(((BigDecimal) tempData[i][2]).doubleValue());
            ordersList.add(orders);
        }


        // Şube objeleri oluşturuluyor
        redBranch = new Branches("KIRMIZI", 41.049792, 29.003031, 10, 20);
        blueBranch = new Branches("MAVİ", 41.049997, 29.026108, 60, 20);
        greenBranch = new Branches("YEŞİL", 41.069940, 29.019250, 15, 35);

        // Şubeler listelere ekleniyor
        branchesList.add(redBranch);
        branchesList.add(blueBranch);
        branchesList.add(greenBranch);
        //Uzaklık hesaplamaları hesaplanıyor
        calculateDistance();
        //Kalan extralar için işlemler çağrılıyor
        callRemain();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        //Şubeler map üzerine işaretleniyor
        placeBranches(mMap, 0.0F, redBranch);
        placeBranches(mMap, 240.0F, blueBranch);
        placeBranches(mMap, 120.0F, greenBranch);

        // Listeler gidecekleri şubelere göre renklendiriliyor
        orderColoring(ordersListRed, 0.0F);
        orderColoring(ordersListGreen, 120.0F);
        orderColoring(ordersListBlue, 240.0F);
    }

    /**
     * Her bir order için map üzerine işaretleme ve renklendirme yapılması için metot çağrılıyor
     * @param orders Sipariş
     * @param color Siparişin hangi şubeden gideceğini gösteren renk
     */
    void orderColoring(List<Orders> orders, float color) {
        for (Orders order : orders) {
            if (order != null)
            placeOrders(mMap, color, order);
        }
    }

    /**
     * Map üzerine şubeler için işaretleme ve renklendirme yapar
     * @param googleMap Map
     * @param color İşaretin/ şubenin rengi
     * @param branchObject Şube
     */
    public void placeBranches(GoogleMap googleMap, float color, Branches branchObject) {

        LatLng branch = new LatLng(branchObject.getLat(), branchObject.getLon());
        mMap.addMarker(new MarkerOptions()
                .position(branch)
                .title(branchObject.getName())
                .icon(BitmapDescriptorFactory.defaultMarker(color)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(branch, 13));
    }

    /**
     * Map üzerine siparişler için işaretleme ve renklendirme yapar
     * @param googleMap Map
     * @param color Siparişin rengi
     * @param orders Sipariş
     */
    public void placeOrders(GoogleMap googleMap, float color, Orders orders) {

        LatLng branch = new LatLng(orders.getLat(), orders.getLon());
        mMap.addMarker(new MarkerOptions()
                .position(branch)
                .title(String.valueOf("Sipariş Numarası: " + orders.getId()))
                .icon(BitmapDescriptorFactory.defaultMarker(color)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(branch));


    }

    /**
     * Siparişin 3 şubeye de olan uzaklıklarını hesaplayarak obje üzerinde güncelleme yapar
     * Minimum değerleri bularak listelere atayacak olan metot çağrılır ve minimum sipariş sayısı
     * dağıtımları hesaplanır
     * Kalan extralar için işlemleri yapacak olan metot da burada çağrılarak kalanların da hangi şubeden
     * gideceğine karar verilir
     */
    public void calculateDistance() {
        Computing computing = new Computing();
        for (Orders order : ordersList) {
            order.setDistanceFromBlue(computing.getDistance(order.getLat(), order.getLon(), returnBranch("MAVİ").getLat(), returnBranch("MAVİ").getLon()));
            order.setDistanceFromRed(computing.getDistance(order.getLat(), order.getLon(), returnBranch("KIRMIZI").getLat(), returnBranch("KIRMIZI").getLon()));
            order.setDistanceFromGreen(computing.getDistance(order.getLat(), order.getLon(), returnBranch("YEŞİL").getLat(), returnBranch("YEŞİL").getLon()));

        }
        ordersListRed = listingMethod(ordersList, returnBranch("KIRMIZI"));
        ordersListBlue = listingMethod(ordersList, returnBranch("MAVİ"));
        ordersListGreen = listingMethod(ordersList, returnBranch("YEŞİL"));

        fillRestOfOrders();
    }

    List<Orders> listingMethod(List<Orders> orders, Branches branch) {
        List<Orders> newOrderList;
        Collections.sort(orders, new OrderComparator(branch.getName()));
        newOrderList = new ArrayList<>();
        for (int i = 0; i < branch.getMin(); i++) {
            newOrderList.add(ordersList.get(i));
        }
        orders.removeAll(newOrderList);
        return newOrderList;
    }

    /**
     * Gönderilen şube ismine göre o şube ismini içeren objeyi döndürür
     * @param name
     * @return
     */
    public Branches returnBranch(String name) {
        for (Branches branches : branchesList) {
            if (branches.getName().equals(name)) {
                return branches;
            }
        }

        return null;
    }

    /**
     * Verilen obje değişkenine göre listeyi sıralar
     */
    public class OrderComparator implements Comparator<Orders> {
        String type = "";

        public OrderComparator(String type) {
            this.type = type;
        }

        public int compare(Orders left, Orders right) {
            if (type.equals("KIRMIZI")) {
                return left.getDistanceFromRed().compareTo(right.getDistanceFromRed());
            } else if (type.equals("MAVİ")) {
                return left.getDistanceFromBlue().compareTo(right.getDistanceFromBlue());
            } else if (type.equals("YEŞİL")) {
                return left.getDistanceFromGreen().compareTo(right.getDistanceFromGreen());
            }

            return 0;
        }
    }

    /**
     * Kalan extralar için şubesini bulacak olan metodu çağırır
     */
    public void fillRestOfOrders() {
        for (Orders order : ordersList) {
            findTheRestBranch(order);
            //  ordersList.remove(order);

        }
    }

    /**
     * Kalanlar için yeniden minimum distance hesaplanır.
     * Şube kotasını kontrol eden metoda gönderilerek metottan gelen şube objesine göre siparişi
     * şubeye atar
     * @param orders Sipariş
     */
    public void findTheRestBranch(Orders orders) {
        punishment(orders);
        double minDistance = Math.min(Math.min(orders.getDistanceFromBlue(), orders.getDistanceFromGreen()), orders.getDistanceFromRed());
        Branches branch = fromMinToBranch(orders, minDistance);
        branch.setRemain(branch.getRemain() - 1);
        orders.setFromBranch(branch);
    }


    /**
     * Kota kalmaması durumunda kotası kalmayan şubeye göre olan uzaklığa +10 eklenir
     * @param orders
     */
    public void punishment(Orders orders) {
        for (Branches branches : branchesList) {
            if(branches.getRemain()==0) {
                if (branches.getName().equals("KIRMIZI")) {
                    orders.setDistanceFromRed(orders.getDistanceFromRed() + 10.0);
                    orders.setGetPunishmentRed(true);
                } else if (branches.getName().equals("MAVİ")) {
                    orders.setDistanceFromBlue(orders.getDistanceFromBlue() + 10.0);
                    orders.setGetPunishmentBlue(true);
                } else if (branches.getName().equals("YEŞİL")) {
                    orders.setDistanceFromGreen(orders.getDistanceFromGreen() + 10.0);
                    orders.setGetPunishmentGreen(true);
                }
            }
        }
    }

    /**
     *
     * @param order Sipariş
     * @param minDistance en kısa mesafe
     * @return en yakın mesafedeki şube
     */
    public Branches fromMinToBranch(Orders order, Double minDistance) {
        Branches branch = new Branches();
        if (order.getDistanceFromRed().doubleValue() == minDistance) {
            branch = returnBranch("KIRMIZI");
            ordersListRed.add(order);
        } else if (order.getDistanceFromBlue().doubleValue() == minDistance) {
            ordersListBlue.add(order);
            branch = returnBranch("MAVİ");
        } else if (order.getDistanceFromGreen().doubleValue() == minDistance) {
            ordersListGreen.add(order);
            branch = returnBranch("YEŞİL");
        }

        return branch;
    }

    /**
     * Her sipariş dağıtıldıktan sonra son kontrolleri yaapar
     */
    public void callRemain() {
        for (Branches branches : branchesList) {
            if (branches.getRemain() == 0) {
                orderThings(branches);
            }
        }
    }

    public void orderThings(Branches branch) {
        double minDistance = 0.0;
        for (Orders order : ordersList) {
           if (order.isGetPunishmentBlue()) {
                order.setDistanceFromBlue(order.getDistanceFromBlue() - 10.0);
            }
            if (order.isGetPunishmentBlue()) {
                order.setDistanceFromBlue(order.getDistanceFromBlue() - 10.0);
            }
            if (order.isGetPunishmentBlue()) {
                order.setDistanceFromBlue(order.getDistanceFromBlue() - 10.0);
            }

            minDistance = Math.min(Math.min(order.getDistanceFromBlue(), order.getDistanceFromGreen()), order.getDistanceFromRed());

            if (findFromDistance(minDistance, order).getName().equals(branch.getName())) {
                checkList.add(order);
            }
        }
    }

    /**
     * En yakın mesafeye sahip olan şubeyi bulur
     * @param distance mesafe
     * @param order sipariş
     * @return branch - şube
     */
    public Branches findFromDistance(double distance, Orders order) {
        Branches branches = new Branches();
        if (order.getDistanceFromRed() == distance) {
            branches = returnBranch("KIRMIZI");
        } else if (order.getDistanceFromBlue() == distance) {
            branches = returnBranch("MAVİ");
        } else if (order.getDistanceFromGreen() == distance) {
            branches = returnBranch("YEŞİL");
        }

        return branches;
    }


}