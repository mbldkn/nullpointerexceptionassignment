package comh.example.melikesardogan.ciceksepeti;

/**
 * Çiçek şubesi sınıfı
 */
public class Branches {


    /**
     * Şube ismi
     */
    private String name = "";
    /**
     * Şube enlemi
     */
    private Double lat = 0.00;
    /**
     * Şube boylamı
     */
    private Double lon = 0.00;
    /**
     * Şubenin max-min kotası
     */
    private int remains = 0;
    /**
     * Şubenin min dağıtması gereken çiçek sayısı
     */
    private int min = 0;

    public Branches(String name, Double lat, Double lon, int remains, int min) {
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.remains = remains;
        this.min = min;
    }

    public Branches() {
    }

    public int getRemain() {
        return remains;
    }

    public void setRemain(int remains) {
        this.remains = remains;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
