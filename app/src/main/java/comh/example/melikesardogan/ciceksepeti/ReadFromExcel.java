package comh.example.melikesardogan.ciceksepeti;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import jxl.Cell;
import jxl.CellType;
import jxl.LabelCell;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Excel işlemlerini sağlayacak olan class
 */
public class ReadFromExcel {
    /**
     * İşlem görecek dosya
     */
    private File file;
    /**
     * Okunan verier
     */
    private Object[][] data;
    /**
     * Sürun isimleri (İlk Satır)
     */
    private String[] columnNames;
    /**
     * inputStream
     */
    private InputStream inputStream;

    public ReadFromExcel(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * @return sütun isimleri
     */
    public String[] getColumnNames() {
        return columnNames;
    }

    /**
     * Dosyadaki verilerin okunmasını ve data objesine atılmasını sağlar
     * @return data
     */
    public Object[][] getData() {


        try {
            WorkbookSettings ws = new WorkbookSettings();
            ws.setEncoding("Cp1252");
            Workbook w = Workbook.getWorkbook(inputStream);
            Sheet sheet = w.getSheet(0);
            data = new Object[sheet.getRows() - 1][sheet.getColumns()];
            columnNames = new String[sheet.getColumns()];

            for (int col = 0; col < sheet.getColumns(); col++) {
                for (int row = 0; row < sheet.getRows(); row++) {
                    if (row == 0) {
                        Cell cell = sheet.getCell(col, row);
                        columnNames[col] = cell.getContents();

                    } else {
                        Cell cell = sheet.getCell(col, row);
                        if (cell.getType() == CellType.NUMBER) {
                            NumberCell nc = (NumberCell) cell;
                            data[row - 1][col] = BigDecimal.valueOf(nc.getValue());
                        } else if (cell.getType() == CellType.ERROR) {
                            data[row - 1][col] = "#HATA";
                        }else if(cell.getType() == CellType.LABEL) {
                            LabelCell lc = (LabelCell) cell;
                            data[row-1][col] = (String)lc.getString().replaceAll("'", "");
                        } else if (cell.getType() == CellType.EMPTY) {
                            data[row - 1][col] = null;
                        } else {
                            data[row - 1][col] = (Object) cell.getContents();
                        }
                    }
                }
            }
        } catch (BiffException | IOException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }

        return data;
    }
}
