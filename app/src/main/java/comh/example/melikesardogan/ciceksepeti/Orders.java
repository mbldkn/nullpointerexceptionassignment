package comh.example.melikesardogan.ciceksepeti;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Sipariş classı
 */
public class Orders {
    /**
     * Sipariş enlemi
     */
    private Double lat = 0.00;
    /**
     * Sipariş boylamı
     */
    private Double lon = 0.00;
    /**
     * Siparişin kırmızı şubeye olan uzaklığı
     */
    private Double distanceFromRed = 0.00;
    /**
     * Siparişin mavi şubeye olan uzaklığı
     */
    private Double distanceFromBlue = 0.00;
    /**
     * Siparişin yeşil şubeye olan uzaklığı
     */
    private Double distanceFromGreen = 0.00;
    /**
     * Siparişin hangi şubeden gönderileceği
     */
    private Branches fromBranch;
    /**
     * Kırmızı şube için ceza alıp almadığı
     * (Kırmızı şubenin bitmesi durumunda distance'a ekleme yapılacak)
     */
    private boolean getPunishmentRed = false;
    /**
     * Mavi şube için ceza alıp almadığı
     * (Mavi şubenin bitmesi durumunda distance'a ekleme yapılacak)
     */
    private boolean getPunishmentBlue = false;
    /**
     * Yeşil şube için ceza alıp almadığı
     * (yeşil şubenin bitmesi durumunda distance'a ekleme yapılacak)
     */
    private boolean getPunishmentGreen = false;

    public boolean isGetPunishmentRed() {
        return getPunishmentRed;
    }

    public void setGetPunishmentRed(boolean getPunishmentRed) {
        this.getPunishmentRed = getPunishmentRed;
    }

    public boolean isGetPunishmentBlue() {
        return getPunishmentBlue;
    }

    public void setGetPunishmentBlue(boolean getPunishmentBlue) {
        this.getPunishmentBlue = getPunishmentBlue;
    }

    public boolean isGetPunishmentGreen() {
        return getPunishmentGreen;
    }

    public void setGetPunishmentGreen(boolean getPunishmentGreen) {
        this.getPunishmentGreen = getPunishmentGreen;
    }

    private int id = 0;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getDistanceFromRed() {
        return distanceFromRed;
    }

    public void setDistanceFromRed(Double distanceFromRed) {
        this.distanceFromRed = distanceFromRed;
    }

    public Double getDistanceFromBlue() {
        return distanceFromBlue;
    }

    public void setDistanceFromBlue(Double distanceFromBlue) {
        this.distanceFromBlue = distanceFromBlue;
    }

    public Double getDistanceFromGreen() {
        return distanceFromGreen;
    }

    public void setDistanceFromGreen(Double distanceFromGreen) {
        this.distanceFromGreen = distanceFromGreen;
    }

    public Branches getFromBranch() {
        return fromBranch;
    }

    public void setFromBranch(Branches fromBranch) {
        this.fromBranch = fromBranch;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    void  fromString(){
        String datas = "";
    }
}
