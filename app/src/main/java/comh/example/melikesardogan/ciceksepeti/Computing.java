package comh.example.melikesardogan.ciceksepeti;

/**
 * Uzaklığı hesaplayan class
 */
public class Computing {

    private double deg;

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }

    public Computing() {
    }

    /**
     * Verilen koordinatlar arası uzaklığın hesaplamması
     * @param lat1 Enlem 1
     * @param lon1 Boylam 1
     * @param lat2 Enlem 2
     * @param lon2 Boylam 2
     * @return d / aradaki mesafe
     */
    public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double radius = 6371; // Radius of the earth in km
        double dLat = degToRad(lat2 - lat1);  // getDistance below
        double dLon = degToRad(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(degToRad(lat1)) * Math.cos(degToRad(lat2))
                * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = radius * c; // Distance in km
        return d;
    }

    public double degToRad(double deg) {
        return deg * (Math.PI / 180);
    }
}
